package com.example.ecommerceapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class AdminMaintainProductActivity extends AppCompatActivity {

    private Button applychange,Deletebtn;
    private EditText name,price,description;
    private ImageView imageview;
    private String productId = "";
    private DatabaseReference productRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_maintain_product);
        productId = getIntent().getStringExtra("pid");
productRef = FirebaseDatabase.getInstance().getReference().child("Products").child(productId);
        applychange = findViewById(R.id.Apply_change);
        name = findViewById(R.id.change_user_product_name);
        price = findViewById(R.id.change_user_product_price);
        description = findViewById(R.id.change_user_product_description);
        imageview = findViewById(R.id.change_user_product_image);
        Deletebtn = findViewById(R.id.delete);

displayspecificinfo();

applychange.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       applychanges();
    }
});
Deletebtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       deletethisproduct();

    }
});

    }
    private void deletethisproduct()
    {
        productRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Toast.makeText(AdminMaintainProductActivity.this,"product deleted successfully",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(AdminMaintainProductActivity.this,AdminCategoryActivity.class));
                    finish();
                }
            }
        });
    }
    private void displayspecificinfo()
    {
        productRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    String pname = dataSnapshot.child("pname").getValue().toString();
                    String pPrice = dataSnapshot.child("price").getValue().toString();

                    String pdescription = dataSnapshot.child("description").getValue().toString();

                    String pimage = dataSnapshot.child("image").getValue().toString();

                    name.setText(pname);
                    price.setText(pPrice);
                    description.setText(pdescription);
                    Picasso.get().load(pimage).into(imageview);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private  void  applychanges()
    {
        String pName = name.getText().toString();
        String pPrice = price.getText().toString();
        String pdescription = description.getText().toString();
if(pName.equals(""))
{
    Toast.makeText(this,"Enter name",Toast.LENGTH_LONG).show();
}else if(pPrice.equals(""))
{
    Toast.makeText(this,"Enter price",Toast.LENGTH_LONG).show();
}
else if(pdescription.equals(""))
{
    Toast.makeText(this,"Enter description",Toast.LENGTH_LONG).show();
}
else{
    HashMap<String,Object> productMap = new HashMap<>();
    productMap.put("pid", productId);


    productMap.put("description",pdescription);


    productMap.put("price",pPrice);
    productMap.put("pname",pName);
    productRef.updateChildren(productMap).addOnCompleteListener(new OnCompleteListener<Void>() {
        @Override
        public void onComplete(@NonNull Task<Void> task) {
            if(task.isSuccessful())
            {
                Toast.makeText(AdminMaintainProductActivity.this,"Successfully edited",Toast.LENGTH_LONG).show();
                startActivity(new Intent(AdminMaintainProductActivity.this,AdminCategoryActivity.class));
                finish();
            }
        }
    });
}






    }
}
