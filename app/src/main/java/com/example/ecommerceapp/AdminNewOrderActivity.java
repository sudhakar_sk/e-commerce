package com.example.ecommerceapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ecommerceapp.Model.AdminOrder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdminNewOrderActivity extends AppCompatActivity {
 private RecyclerView orderlist;
 private DatabaseReference orderref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_new_order);
        orderref = FirebaseDatabase.getInstance().getReference().child("Orders");
        orderlist = findViewById(R.id.order_list);
        orderlist.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<AdminOrder> options = new FirebaseRecyclerOptions.Builder<AdminOrder>()
                .setQuery(orderref,AdminOrder.class)
                .build();
        FirebaseRecyclerAdapter<AdminOrder,AdminOrdersViewHolder> adapter = new FirebaseRecyclerAdapter<AdminOrder, AdminOrdersViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull AdminOrdersViewHolder adminOrdersViewHolder, final int k, @NonNull final AdminOrder adminOrder) {
adminOrdersViewHolder.userName.setText("Name:"+adminOrder.getName());
                adminOrdersViewHolder.userphone.setText("Phoneno:"+adminOrder.getPhone());
                adminOrdersViewHolder.totalprice.setText("Totalamount:"+adminOrder.getTotalamount());
                adminOrdersViewHolder.datetime.setText("order at :"+adminOrder.getDate()+" "+adminOrder.getTime());
                adminOrdersViewHolder.address.setText("shipment address"+adminOrder.getAddress()+" "+adminOrder.getCity());
                adminOrdersViewHolder.showbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String uID = getRef(k).getKey();
                        startActivity(new Intent(AdminNewOrderActivity.this,AdminshowproductActivity.class).putExtra("uid",uID));
                    }
                });

                adminOrdersViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CharSequence options[] = new CharSequence[]
                                {
                                        "Yes",
                                        "NO"
                                };
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdminNewOrderActivity.this);
                        builder.setTitle("have you shipped this order?");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(i==0)
                                {
                                    String uID = getRef(k).getKey();
                                    RemoveOrder(uID);
                                }else
                                {
                                    finish();
                                }
                            }
                        });
builder.show();
                    }
                });


            }

            private void RemoveOrder(String uID)
            {
               orderref.child(uID).removeValue();
            }

            @NonNull
            @Override
            public AdminOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout,parent,false);
                return new AdminOrdersViewHolder(view);
            }
        };
        orderlist.setAdapter(adapter);
        adapter.startListening();

    }


    public  static  class AdminOrdersViewHolder extends RecyclerView.ViewHolder
    {
        public TextView userName,userphone,totalprice,address,datetime;
        public Button showbtn;

        public AdminOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.user_name);
            userphone = itemView.findViewById(R.id.order_phone_no);
            totalprice = itemView.findViewById(R.id.order_price);
            address = itemView.findViewById(R.id.order_address_city);
            datetime = itemView.findViewById(R.id.order_Date_time);
            showbtn = itemView.findViewById(R.id.show_all);

        }
    }
}
