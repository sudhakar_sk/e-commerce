package com.example.ecommerceapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommerceapp.Model.Cart;
import com.example.ecommerceapp.ViewHolder.CartViewHolder;
import com.example.ecommerceapp.prevalent.prevalent;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CartActivity extends AppCompatActivity {

    private RecyclerView recycleview;
    private RecyclerView.LayoutManager layoutManager;
    private Button nextProcessbtn;
    private TextView txtmsg1;
    private TextView txtTotalAmount;
    private int overallprice=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
       txtmsg1 = findViewById(R.id.msg1);
        recycleview = findViewById(R.id.cart_list);
        recycleview.setHasFixedSize(true);

        layoutManager   = new LinearLayoutManager(this);
        recycleview.setLayoutManager(layoutManager);
        nextProcessbtn = findViewById(R.id.next_process_btn);
        txtTotalAmount =findViewById(R.id.total_price);
        nextProcessbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                txtTotalAmount.setText("Total Price = "+String.valueOf(overallprice));
                Intent intent = new Intent(CartActivity.this,ConfirmFinalOrderActivity.class);
                intent.putExtra("Total price", String.valueOf(overallprice));

                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

checkOrderstate();
        final DatabaseReference cartref = FirebaseDatabase.getInstance().getReference().child("Cartlist");
        FirebaseRecyclerOptions<Cart> options = new FirebaseRecyclerOptions.Builder<Cart>().setQuery(cartref.child("User View").child(prevalent.currentonlineuser.getPhone()).child("Products"),Cart.class)
                .build();
        FirebaseRecyclerAdapter<Cart, CartViewHolder> adapter = new FirebaseRecyclerAdapter<Cart, CartViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull CartViewHolder cartViewHolder, int i, @NonNull final Cart cart) {
                cartViewHolder.txtproductQuantity.setText("Quantity = "+cart.getQuantity());
                cartViewHolder.txtproductprice.setText("Price = "+(cart.getPrice()));
                cartViewHolder.txtProductName.setText((cart.getPname()));

                int oneproductprice = ((Integer.valueOf(cart.getPrice()))*Integer.valueOf(cart.getQuantity()));
                overallprice = overallprice+oneproductprice;
                cartViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CharSequence options[] = new CharSequence[]
                                {
                                        "Edit",
                                        "Remove"
                                };
                        AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
                        builder.setTitle("Cart Options:");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                              if(i==0)
                              {
                                  Intent intent = new Intent(CartActivity.this,ProductDetailActivity.class);
                                  intent.putExtra("pid",cart.getPid());
                                  startActivity(intent);
                              }
                                if(i==1)
                                {
                                      cartref.child("User View")
                                              .child(prevalent.currentonlineuser.getPhone())
                                              .child("Products")
                                              .child(cart.getPid())
                                              .removeValue()
                                              .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                  @Override
                                                  public void onComplete(@NonNull Task<Void> task) {
                                                      if(task.isSuccessful())
                                                      {
                                                          Toast.makeText(CartActivity.this,"Item removed",Toast.LENGTH_LONG).show();
                                                          Intent intent = new Intent(CartActivity.this,HomeActivity.class);
                                                          startActivity(intent);
                                                      }

                                                  }
                                              });
                                }
                            }
                        });
                        builder.show();

                    }
                });
            }

            @NonNull
            @Override
            public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_layout,parent,false);
                CartViewHolder holder = new CartViewHolder(view);
                return holder;
            }
        };
        recycleview.setAdapter(adapter);
        adapter.startListening();

    }
    private void checkOrderstate()
    {
        DatabaseReference orderRef;
        orderRef = FirebaseDatabase.getInstance().getReference().child("Orders").child(prevalent.currentonlineuser.getPhone());
        orderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    String shippingstate = dataSnapshot.child("state").getValue().toString();
                    String username = dataSnapshot.child("name").getValue().toString();
                    if(shippingstate.equals("shipped"))
                    {
                        txtTotalAmount.setText("Dear"+username+"\n order is shipped");
                        recycleview.setVisibility(View.GONE);
                        txtmsg1.setVisibility(View.VISIBLE);
                        txtmsg1.setText("final order shipped successfully");
                        nextProcessbtn.setVisibility(View.GONE);
                        Toast.makeText(CartActivity.this,"you can purchase more products",Toast.LENGTH_LONG).show();
                    }
                    else if(shippingstate.equals("not shipped"))
                    {

                        txtTotalAmount.setText("Shipping state = Not shipped");
                        recycleview.setVisibility(View.GONE);
                        txtmsg1.setVisibility(View.VISIBLE);
                        nextProcessbtn.setVisibility(View.GONE);
                        txtmsg1.setText("final order placed successfully");
                        Toast.makeText(CartActivity.this,"you can purchase more products",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
