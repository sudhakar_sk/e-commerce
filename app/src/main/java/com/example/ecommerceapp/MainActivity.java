package com.example.ecommerceapp;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ecommerceapp.Model.Users;
import com.example.ecommerceapp.prevalent.prevalent;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
    private Button joinnow,loginbtn;
    String parentname ="Users";
    ProgressDialog loadingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        joinnow=(Button)findViewById(R.id.main_join_now_btn);
        loginbtn=(Button)findViewById(R.id.main_login_btn);
        loadingBar = new ProgressDialog(this);
        Paper.init(this);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,loginActivity.class);
                startActivity(intent);
            }
        });
        joinnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        String Userphonekey = Paper.book().read(prevalent.userphonekey);
        String UserPasswordKey = Paper.book().read(prevalent.userpasswordkey);

        if(Userphonekey !="" && UserPasswordKey != "")
        {
            if(!TextUtils.isEmpty(Userphonekey) && !TextUtils.isEmpty(UserPasswordKey))
            {
                Allowaccess(Userphonekey,UserPasswordKey);
                loadingBar.setTitle("already logged in");
                loadingBar.setMessage("please wait....");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();
            }

        }
    }
    private void Allowaccess(final String phno, final String password) {
        FirebaseApp.initializeApp(this);
        final DatabaseReference RootRef;

        RootRef = FirebaseDatabase.getInstance().getReference();
        RootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(parentname).child(phno).exists()) {
                    Users userdata = dataSnapshot.child(parentname).child(phno).getValue(Users.class);
                    assert userdata != null;
                    if (userdata.getPhone().equals(phno)) {
                        if (userdata.getPassword().equals(password)) {

                            Toast.makeText(MainActivity.this, "please wait you have already logged in", Toast.LENGTH_LONG).show();
                            loadingBar.dismiss();
                            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                            prevalent.currentonlineuser = userdata;
                            startActivity(intent);

                        } else {
                            Toast.makeText(MainActivity.this, "password is incorrect", Toast.LENGTH_LONG).show();
                            loadingBar.dismiss();
                        }
                    }
                }

                else
                {
                    Toast.makeText(MainActivity.this,"Account with this "+ phno+" does not exists",Toast.LENGTH_LONG).show();
                    loadingBar.dismiss();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
