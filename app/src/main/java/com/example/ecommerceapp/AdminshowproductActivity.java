package com.example.ecommerceapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ecommerceapp.Model.Cart;
import com.example.ecommerceapp.ViewHolder.CartViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdminshowproductActivity extends AppCompatActivity {
    private RecyclerView product_list;
    RecyclerView.LayoutManager layoutManager;
    private DatabaseReference cartlistref;
    private String userId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminshowproduct);
        userId = getIntent().getStringExtra("uid");
        product_list = findViewById(R.id.product_list_view);
        product_list.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        product_list.setLayoutManager(layoutManager);
        cartlistref = FirebaseDatabase.getInstance().getReference().child("Cartlist").child("Admin View").child(userId).child("Products");


    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<Cart> options = new FirebaseRecyclerOptions.Builder<Cart>()
                .setQuery(cartlistref,Cart.class)
                .build();
        FirebaseRecyclerAdapter<Cart, CartViewHolder> adapter = new FirebaseRecyclerAdapter<Cart, CartViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull CartViewHolder cartViewHolder, int i, @NonNull Cart cart) {

                cartViewHolder.txtproductQuantity.setText("Quantity = "+cart.getQuantity());
                cartViewHolder.txtproductprice.setText("Price = "+(cart.getPrice()));
                cartViewHolder.txtProductName.setText((cart.getPname()));
                Toast.makeText(AdminshowproductActivity.this,"successful",Toast.LENGTH_LONG).show();
            }

            @NonNull
            @Override
            public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_layout,parent,false);
                CartViewHolder holder = new CartViewHolder(view);
                return holder;
            }
        };
        product_list.setAdapter(adapter);
        adapter.startListening();

    }
}
