package com.example.ecommerceapp;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private Button Createaccountbutton;
    private EditText inputname,inputphoneno,inputpassword;
    private ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Createaccountbutton = (Button)findViewById(R.id.register_btn);
        inputname = (EditText) findViewById(R.id.register_username_input);
        inputphoneno = (EditText)findViewById(R.id.register_phone_number_input);
        inputpassword = (EditText)findViewById(R.id.register_password_input);
        loadingBar = new ProgressDialog(this);
        Createaccountbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateAccount();

            }
        });

    }
    private void CreateAccount()
    {
        String name = inputname.getText().toString();
        String phno = inputphoneno.getText().toString();
        String password = inputpassword.getText().toString();
        if(TextUtils.isEmpty(name))
        {
            Toast.makeText(this,"please enter the name",Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(phno))
        {
           Toast.makeText(this,"please enter the phno",Toast.LENGTH_LONG).show();
           
        }

        else{
            if(TextUtils.isEmpty(password))
            {
                Toast.makeText(this,"please enter the password",Toast.LENGTH_LONG).show();
            }
            else
            {
            Toast.makeText(this,"please enter the correctly",Toast.LENGTH_LONG).show();
            loadingBar.setTitle("Create Account");
            loadingBar.setMessage("please wait,while we are checking the credentials");
            loadingBar.setCanceledOnTouchOutside(false);
            loadingBar.show();
            ValidatephoneNumber(name,phno,password);
        }}




    }
    private void ValidatephoneNumber(final String name, final String phno, final String password)
    {
        FirebaseApp.initializeApp(this);
        final DatabaseReference RootRef;

        RootRef = FirebaseDatabase.getInstance().getReference();
        RootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!(dataSnapshot.child("Users").child(phno).exists()))
                {
                    HashMap<String,Object> userdataMap = new HashMap<>();
                    userdataMap.put("phone",phno);
                    userdataMap.put("name",name);
                    userdataMap.put("password",password);
                    RootRef.child("Users").child(phno).updateChildren(userdataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                            {
                                Toast.makeText(RegisterActivity.this,"Congratulation your account is successfully created",Toast.LENGTH_LONG).show();
                                loadingBar.dismiss();
                                Intent intent = new Intent(RegisterActivity.this,loginActivity.class);
                                startActivity(intent);
                            }
                            else
                            {
                                loadingBar.dismiss();
                                Toast.makeText(RegisterActivity.this,"Network Error: please try again after some time", Toast.LENGTH_LONG).show();

                            }
                        }
                    });

                }
                else {
                    Toast.makeText(RegisterActivity.this, "This " + phno + " already exists", Toast.LENGTH_LONG).show();
                    loadingBar.dismiss();
                    Toast.makeText(RegisterActivity.this, "please try again using another phno", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {



                    Toast.makeText(RegisterActivity.this,"database error", Toast.LENGTH_LONG).show();
                }
        });
    }
}
