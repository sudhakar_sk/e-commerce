package com.example.ecommerceapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.ecommerceapp.Model.Products;
import com.example.ecommerceapp.prevalent.prevalent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class ProductDetailActivity extends AppCompatActivity {

    private FloatingActionButton addtocart;
    private ImageView productimage;
    private ElegantNumberButton numberbutton;
    private TextView productprice,productdescription,productName;
    private String productid="",state="Normal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        productid = getIntent().getStringExtra("pid");
        addtocart = findViewById(R.id.add_productto_cart);
        numberbutton = findViewById(R.id.number_btn);
        productimage = findViewById(R.id.product_image_detail);
        productName = findViewById(R.id.product_name_detail);
        productdescription = findViewById(R.id.product_description_detail);
        productprice = findViewById(R.id.product_price_detail);
        getproductdetail(productid);

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(state.equals("Order placed")|| state.equals("Order shipped"))
                {Toast.makeText(ProductDetailActivity.this,"you can purchase more product when your order is confirmed",Toast.LENGTH_LONG).show();}
                else
                {
                    addingtocart();
                }
            }
        });
    }
    private void addingtocart()
    {
        String savecurrentDate,savecurrenttime;
        Calendar calfordate = Calendar.getInstance();
        SimpleDateFormat currentdate = new SimpleDateFormat("MMM dd,yyyy");
        savecurrentDate =currentdate.format(calfordate.getTime());
        SimpleDateFormat currenttime = new SimpleDateFormat("HH:mm:ss a");
        savecurrenttime =currenttime.format(calfordate.getTime());

        final DatabaseReference cartref = FirebaseDatabase.getInstance().getReference().child("Cartlist");
        final HashMap<String,Object> cartmap = new HashMap<>();
        cartmap.put("pid",productid);
        cartmap.put("pname",productName.getText().toString());
        cartmap.put("price",productprice.getText().toString());
        cartmap.put("date",savecurrentDate);
        cartmap.put("time",savecurrenttime);
        cartmap.put("quantity",numberbutton.getNumber());
        cartmap.put("discount","");
        cartref.child("User View").child(prevalent.currentonlineuser.getPhone()).child("Products").child(productid).updateChildren(cartmap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            cartref.child("Admin View").child(prevalent.currentonlineuser.getPhone()).child("Products").child(productid).updateChildren(cartmap)
                                 .addOnCompleteListener(new OnCompleteListener<Void>() {
                                     @Override
                                     public void onComplete(@NonNull Task<Void> task) {
                                         if(task.isSuccessful())
                                         {
                                             Toast.makeText(ProductDetailActivity.this,"added to cart",Toast.LENGTH_LONG).show();
                                             Intent intent = new Intent(ProductDetailActivity.this,HomeActivity.class);
                                             startActivity(intent);
                                         }
                                     }
                                 });
                        }
                    }
                });



    }
    private  void getproductdetail(String productid)
    {
        DatabaseReference productref = FirebaseDatabase.getInstance().getReference().child("Products");
        productref.child(productid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    Products product = dataSnapshot.getValue(Products.class);
                    productName.setText(product.getPname());
                    productprice.setText(product.getPrice());
                    productdescription.setText(product.getDescription());
                    Picasso.get().load(product.getImage()).into(productimage);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkOrderstate();
    }

    private void checkOrderstate()
    {
        DatabaseReference orderRef;
        orderRef = FirebaseDatabase.getInstance().getReference().child("Orders").child(prevalent.currentonlineuser.getPhone());
        orderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String shippingstate = dataSnapshot.child("state").getValue().toString();
                    String username = dataSnapshot.child("name").getValue().toString();
                    if(shippingstate.equals("shipped"))
                    {
                        state = "Order shipped";

                    }else if(shippingstate.equals("not shipped"))
                    {
                        state ="Order placed";
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }}