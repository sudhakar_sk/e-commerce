package com.example.ecommerceapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class AddnewproductActivity extends AppCompatActivity {
 private String categoryName,Description,price,pname,savecurentdate,savecurrenttime;
 private Button Addnewproduct;
 private ImageView inputProductImage;
 private static final int Gallarypick=1;
 private Uri imageuri;
 private EditText InputProductname,InputProductdescription,Inputproductprice;
 private StorageReference productImagesref;
 private String randomkey,downloadimageurl;
 private DatabaseReference productref;
 private ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewproduct);
        loadingBar = new ProgressDialog(this);

        categoryName = getIntent().getExtras().get("category").toString();
        productImagesref= FirebaseStorage.getInstance().getReference().child("Product Images");
        productref=FirebaseDatabase.getInstance().getReference().child("Products");
        Toast.makeText(AddnewproductActivity.this,categoryName,Toast.LENGTH_LONG).show();
   Addnewproduct = (Button)findViewById(R.id.add_new_product);
   InputProductname = (EditText)findViewById(R.id.product_name);
        InputProductdescription = (EditText)findViewById(R.id.product_description);
   inputProductImage =(ImageView)findViewById(R.id.select_product_image);
   Inputproductprice =(EditText)findViewById(R.id.product_price);
   Addnewproduct.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           validateproduct();
       }


   });
   inputProductImage.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           OpenGallery();
       }


   });

    }
    private void validateproduct() {
        Description = InputProductdescription.getText().toString();
        price = Inputproductprice.getText().toString();
        pname = InputProductname.getText().toString();

        if(imageuri == null)
        {
            Toast.makeText(AddnewproductActivity.this,"image is mandatory",Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(Description))
        {
            Toast.makeText(this,"fill description",Toast.LENGTH_LONG).show();
        } else if(TextUtils.isEmpty(price))
        {
            Toast.makeText(this,"fill price",Toast.LENGTH_LONG).show();
        } else if(TextUtils.isEmpty(pname))
        {
            Toast.makeText(this,"fill productname",Toast.LENGTH_LONG).show();
        }
        else
        {
            storeproductinfo();
        }

    }
    private  void storeproductinfo()
    {
        loadingBar.setTitle("creating product");
        loadingBar.setMessage("please wait");
        loadingBar.setCanceledOnTouchOutside(false);
        loadingBar.show();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        savecurentdate = currentDate.format(calendar.getTime());
        SimpleDateFormat currenttime = new SimpleDateFormat("HH:mm:ss a");
        savecurrenttime = currenttime.format(calendar.getTime());
        randomkey = savecurentdate+savecurrenttime;
        final StorageReference filepath = productImagesref.child(imageuri.getLastPathSegment()+ randomkey+".jpg");
        final UploadTask uploadTask = filepath.putFile(imageuri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
               String message = e.toString();
               Toast.makeText(AddnewproductActivity.this,"Error"+message,Toast.LENGTH_LONG).show();
               loadingBar.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(AddnewproductActivity.this,"image uploaded successfully",Toast.LENGTH_LONG).show();
                Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if(!task.isSuccessful())
                        {
                            throw task.getException();
                        }
                        downloadimageurl = filepath.getDownloadUrl().toString();
                        return filepath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful())
                        {
                            downloadimageurl = task.getResult().toString();
                            Toast.makeText(AddnewproductActivity.this,"got download uri",Toast.LENGTH_LONG).show();
                            saveproductinfotodb();

                        }

                    }
                });

            }
            private  void  saveproductinfotodb()
            {
                HashMap<String,Object>productMap = new HashMap<>();
                productMap.put("pid", randomkey);
                productMap.put("date", savecurentdate);
                productMap.put("time", savecurrenttime);
                productMap.put("description",Description);
                productMap.put("image",downloadimageurl);
                productMap.put("category",categoryName);
                productMap.put("price",price);
                productMap.put("pname",pname);
                productref.child(randomkey).updateChildren(productMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            Intent intent = new Intent(AddnewproductActivity.this, AdminCategoryActivity.class);
                            startActivity(intent);

                            loadingBar.dismiss();
                            Toast.makeText(AddnewproductActivity.this,"product stored successfully",Toast.LENGTH_LONG).show();

                        }
                        else
                        {
                            loadingBar.dismiss();
                            String error = task.getException().toString();
                            Toast.makeText(AddnewproductActivity.this,"error"+error,Toast.LENGTH_LONG).show();

                        }
                    }
                });
            }
        });

    }
    private void OpenGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,Gallarypick);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Gallarypick && resultCode==RESULT_OK && data!=null)
        {
            imageuri= data.getData();
            inputProductImage.setImageURI(imageuri);
        }
    }

}
