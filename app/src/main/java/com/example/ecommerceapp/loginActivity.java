package com.example.ecommerceapp;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommerceapp.Model.Users;
import com.example.ecommerceapp.prevalent.prevalent;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class loginActivity extends AppCompatActivity {
    private ProgressDialog loadingBar;
    private Button login;
    private EditText inputphoneno, inputpassword;
    private TextView admin,notadmin;
    private String parentname = "Users";
    private CheckBox ckboxrememberme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (Button) findViewById(R.id.main_login_btn);
        admin = (TextView)findViewById(R.id.admin_login);
        notadmin = (TextView)findViewById(R.id.notadmin_login);
        inputphoneno = (EditText) findViewById(R.id.login_phone_number_input);
        inputpassword = (EditText) findViewById(R.id.login_password_input);
        loadingBar = new ProgressDialog(this);
        ckboxrememberme = (CheckBox)findViewById(R.id.checkBox2);
        Paper.init(this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAccount();
            }
        });
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login.setText("login admin");
                admin.setVisibility(View.INVISIBLE);
                notadmin.setVisibility(View.VISIBLE);
                parentname = "Admins";
            }
        });
        notadmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login.setText("login");
                admin.setVisibility(View.VISIBLE);
                notadmin.setVisibility(View.INVISIBLE);
                parentname = "Users";
            }
        });

    }

    private void validateAccount() {


        String phno = inputphoneno.getText().toString();
        String password = inputpassword.getText().toString();

        if (TextUtils.isEmpty(phno)) {
            Toast.makeText(this, "please enter the phno", Toast.LENGTH_LONG).show();

        } else {
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "please enter the password", Toast.LENGTH_LONG).show();
            } else {

                loadingBar.setTitle("login Account");
                loadingBar.setMessage("please wait,while we are checking the credentials");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();
                Validate(phno, password);
            }
        }


    }

    private void Validate(final String phno, final String password) {


        if(ckboxrememberme.isChecked())
        {
            Paper.book().write(prevalent.userphonekey,phno);
            Paper.book().write(prevalent.userpasswordkey,password);
        }
        FirebaseApp.initializeApp(this);
        final DatabaseReference RootRef;

        RootRef = FirebaseDatabase.getInstance().getReference();
        RootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(parentname).child(phno).exists())
                {
                    Users userdata = dataSnapshot.child(parentname).child(phno).getValue(Users.class);
                    assert userdata != null;
                    if(userdata.getPhone().equals(phno))
                    {
                        if(userdata.getPassword().equals(password))
                        {
                            if(parentname.equals("Admins")) {
                                Toast.makeText(loginActivity.this, "successfully logged in admin panel", Toast.LENGTH_LONG).show();
                                loadingBar.dismiss();
                                Intent intent = new Intent(loginActivity.this, AdminCategoryActivity.class);
                                startActivity(intent);
                            }
                            else
                            {
                                Toast.makeText(loginActivity.this, "successfully logged in", Toast.LENGTH_LONG).show();
                                loadingBar.dismiss();
                                Intent intent = new Intent(loginActivity.this, HomeActivity.class);
                                prevalent.currentonlineuser = userdata;
                                startActivity(intent);

                            }
                        }
                        else
                        {
                            Toast.makeText(loginActivity.this,"password is incorrect",Toast.LENGTH_LONG).show();
                            loadingBar.dismiss();
                        }
                    }

                }
                else
                {
                    Toast.makeText(loginActivity.this,"Account with this "+ phno+" does not exists",Toast.LENGTH_LONG).show();
                    loadingBar.dismiss();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}