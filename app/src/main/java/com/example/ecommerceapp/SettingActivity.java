package com.example.ecommerceapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommerceapp.prevalent.prevalent;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingActivity extends AppCompatActivity {
    private CircleImageView profileImageview;
    private EditText fullNameEditText,userphoneEditText,adddressEdittext;
    private TextView profilechangeTextbtn, closetextbtn,savetextButton;
    private Uri imageuri;
    private String myurl="";
    private String checker = "";
    private StorageReference StorageRef;
    private StorageTask uploadtask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        profileImageview = (CircleImageView)findViewById(R.id.setting_profile);
        fullNameEditText = (EditText)findViewById(R.id.setting_Fullname);
        userphoneEditText = (EditText)findViewById(R.id.setting_phoneno);
        adddressEdittext = (EditText)findViewById(R.id.setting_address);
        profilechangeTextbtn = (TextView)findViewById(R.id.profile_change_btn);
        closetextbtn = (TextView)findViewById(R.id.close_settings);
        savetextButton= (TextView)findViewById(R.id.update_settings);
        StorageRef = FirebaseStorage.getInstance().getReference().child("Profile pictures");
        userInfoDisplay(profileImageview,fullNameEditText,userphoneEditText,adddressEdittext);
        closetextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        savetextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checker.equals("clicked"))
                {
                    userInfosaved();
                }
                else {
                      updateonlyuserinfo();
                }
            }
        });
        profilechangeTextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checker = "clicked";
                CropImage.activity(imageuri)
                        .setAspectRatio(1,1)
                        .start(SettingActivity.this);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode==RESULT_OK)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageuri = result.getUri();

            profileImageview.setImageURI(imageuri);
        }
        else
        {
            Toast.makeText(SettingActivity.this,"error",Toast.LENGTH_LONG).show();
            startActivity(new Intent(SettingActivity.this,SettingActivity.class));
            finish();
        }
    }

    private  void userInfosaved()
    {
        if(TextUtils.isEmpty(fullNameEditText.getText().toString()))
        {
            Toast.makeText(SettingActivity.this,"enter name",Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(userphoneEditText.getText().toString()))
        {
            Toast.makeText(SettingActivity.this,"enter phno",Toast.LENGTH_LONG).show();
        }  else if(TextUtils.isEmpty(adddressEdittext.getText().toString()))
        {
            Toast.makeText(SettingActivity.this,"enter address",Toast.LENGTH_LONG).show();
        }  else if(checker.equals("clicked"))
        {
            uploadimage();
            Toast.makeText(SettingActivity.this,"enter phno",Toast.LENGTH_LONG).show();
        }
    }
    private  void uploadimage()
    {
        final ProgressDialog loadingBar =new ProgressDialog(this);
        loadingBar.setTitle("Update profile");
        loadingBar.setMessage("please wait,while we are updating the Account");
        loadingBar.setCanceledOnTouchOutside(false);
        loadingBar.show();
        if(imageuri!=null)
        {
            final StorageReference fileref = StorageRef.child(prevalent.currentonlineuser.getPhone() + ".jpg");

            uploadtask = fileref.putFile(imageuri);
            uploadtask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {
                 if(!task.isSuccessful())
                 {
                    throw  task.getException();
                 }
                 return fileref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful())
                    {
                        Uri downloaduri = (Uri) task.getResult();
                        myurl = downloaduri.toString();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users");
                        HashMap<String,Object> userMap = new HashMap<>();
                        userMap.put("name",fullNameEditText.getText().toString());
                        userMap.put("phoneOrder",userphoneEditText.getText().toString());
                        userMap.put("address",adddressEdittext.getText().toString());
                        userMap.put("image",myurl);
                        ref.child(prevalent.currentonlineuser.getPhone()).updateChildren(userMap);
                        Picasso.get().load(prevalent.currentonlineuser.getImage()).placeholder(R.drawable.profile).into(profileImageview);
                        loadingBar.dismiss();
                        startActivity(new Intent(SettingActivity.this,HomeActivity.class));
                        Toast.makeText(SettingActivity.this,"profile info updated successfullly",Toast.LENGTH_LONG).show();
                        finish();
                    }
                    else
                    {
                        loadingBar.dismiss();
                        Toast.makeText(SettingActivity.this,"Error...........",Toast.LENGTH_LONG).show();
                    }

                }
            });
        }
        else
        {
            Toast.makeText(SettingActivity.this,"image not selected",Toast.LENGTH_LONG).show();
        }
    }
    private void updateonlyuserinfo()
    {
        final ProgressDialog loadingBar =new ProgressDialog(this);
        loadingBar.setTitle("Update profile");
        loadingBar.setMessage("please wait,while we are updating the Account");
        loadingBar.setCanceledOnTouchOutside(false);
        loadingBar.show();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users");
        HashMap<String,Object> userMap = new HashMap<>();
        userMap.put("name",fullNameEditText.getText().toString());
        userMap.put("phoneOrder",userphoneEditText.getText().toString());
        userMap.put("address",adddressEdittext.getText().toString());
        ref.child(prevalent.currentonlineuser.getPhone()).updateChildren(userMap);
        loadingBar.dismiss();
        startActivity(new Intent(SettingActivity.this,HomeActivity.class));
    }
    private  void userInfoDisplay(final CircleImageView profileImageview, final EditText fullNameEditText, final EditText userphoneEditText, final EditText adddressEdittext)
    {
        DatabaseReference userref = FirebaseDatabase.getInstance().getReference().child("Users").child(prevalent.currentonlineuser.getPhone());
        userref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    if(dataSnapshot.child("image").exists())
                    {
                        String image = dataSnapshot.child("image").getValue().toString();
                        String name = dataSnapshot.child("name").getValue().toString();
                        String phone = dataSnapshot.child("phone").getValue().toString();
                        String address = dataSnapshot.child("address").getValue().toString();
                        Picasso.get().load(image).into(profileImageview);
                        fullNameEditText.setText(name);
                        userphoneEditText.setText(phone);
                        adddressEdittext.setText(address);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
