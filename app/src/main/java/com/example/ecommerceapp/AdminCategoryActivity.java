package com.example.ecommerceapp;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AdminCategoryActivity extends AppCompatActivity {

    private ImageView tshirts,sportsTshirts, femaleDresses,sweaters;
    private  ImageView glasses,hatscap,walletsBagspurses,shoes;
    private ImageView headphone,laptops,watches,mobilephones;
    private Button logoutbtn,checkorderbtn,maintainorderbtn;
private  String type ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_category);

        logoutbtn =findViewById(R.id.logout_btn);
        maintainorderbtn = findViewById(R.id.maintain_btn);
        checkorderbtn = findViewById(R.id.check_orders_btn);
        logoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            }
        });
        maintainorderbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,HomeActivity.class);
intent.putExtra("Admin","Admin");
startActivity(intent);
                finish();


            }
        });

        checkorderbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AdminNewOrderActivity.class);
                startActivity(intent);


            }
        });

        tshirts = (ImageView)findViewById(R.id.t_shirts);
        sportsTshirts = (ImageView)findViewById(R.id.sports_t_shirts);
        femaleDresses = (ImageView)findViewById(R.id.female);
        sweaters = (ImageView)findViewById(R.id.sweather);
        glasses = (ImageView)findViewById(R.id.glasses);
        hatscap = (ImageView)findViewById(R.id.hats);
        walletsBagspurses = (ImageView)findViewById(R.id.purses_bags);
        shoes = (ImageView)findViewById(R.id.shoess);
        headphone = (ImageView)findViewById(R.id.headphons);
        laptops = (ImageView)findViewById(R.id.laptops);
        watches = (ImageView)findViewById(R.id.watches);
        mobilephones = (ImageView)findViewById(R.id.mobiles);
        tshirts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","tShirts");
                startActivity(intent);
            }
        });
        sportsTshirts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","sportsTShirts");
                startActivity(intent);
            }
        });

        femaleDresses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","femaleDresses");
                startActivity(intent);
            }
        });
        sweaters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","sweaters");
                startActivity(intent);
            }
        });
        glasses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","glasses");
                startActivity(intent);
            }
        });
        hatscap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","hatscap");
                startActivity(intent);
            }
        });
        walletsBagspurses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","walletsBagspurses");
                startActivity(intent);
            }
        });
        shoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","shoes");
                startActivity(intent);
            }
        });
        headphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","headphone");
                startActivity(intent);
            }
        });
        laptops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","laptops");
                startActivity(intent);
            }
        });
        watches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","watches");
                startActivity(intent);
            }
        });
        mobilephones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminCategoryActivity.this,AddnewproductActivity.class);
                intent.putExtra("category","mobilephones");
                startActivity(intent);
            }
        });





    }
}
