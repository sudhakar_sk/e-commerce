package com.example.ecommerceapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ecommerceapp.prevalent.prevalent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class ConfirmFinalOrderActivity extends AppCompatActivity {


    private EditText nameedittext,phoneedittext,addressedittext,cityedittext;
    private Button confirmOrderbtn;
    private String totalamount = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_final_order);
        totalamount = getIntent().getStringExtra("Total price");
        Toast.makeText(ConfirmFinalOrderActivity.this,totalamount,Toast.LENGTH_LONG).show();
        confirmOrderbtn = (Button)findViewById(R.id.confirm_final_order);
        nameedittext = (EditText)findViewById(R.id.shippment_name);
        phoneedittext = (EditText)findViewById(R.id.shippment_phno);
        addressedittext = (EditText)findViewById(R.id.shippment_address);
        cityedittext = (EditText)findViewById(R.id.shippment_city);

        confirmOrderbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check();
            }
        });
    }

    private void check() {
        if (TextUtils.isEmpty(nameedittext.getText().toString())) {
            Toast.makeText(this, "please provide name", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(phoneedittext.getText().toString())) {
            Toast.makeText(this, "please provide phone no", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(addressedittext.getText().toString())) {
            Toast.makeText(this, "please provide address", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(cityedittext.getText().toString())) {
            Toast.makeText(this, "please provide city name", Toast.LENGTH_LONG).show();
        } else
        {
         ConfirmOrder();
    }
    }

    private void ConfirmOrder()
    {
    String savecurrentDate,savecurrenttime;
        Calendar calfordate = Calendar.getInstance();
        SimpleDateFormat currentdate = new SimpleDateFormat("MMM dd,yyyy");
        savecurrentDate =currentdate.format(calfordate.getTime());
        SimpleDateFormat currenttime = new SimpleDateFormat("HH:mm:ss a");
        savecurrenttime =currenttime.format(calfordate.getTime());
        final DatabaseReference orderref = FirebaseDatabase.getInstance().getReference().child("Orders").child(prevalent.currentonlineuser.getPhone());
        HashMap<String,Object> ordermap = new HashMap<>();

        ordermap.put("name",nameedittext.getText().toString());
        ordermap.put("phone",phoneedittext.getText().toString());
        ordermap.put("address",addressedittext.getText().toString());
        ordermap.put("time",savecurrenttime);
        ordermap.put("city",cityedittext.getText().toString());
        ordermap.put("date",savecurrentDate);
        ordermap.put("state","not shipped");
        ordermap.put("totalamount",totalamount);
        orderref.updateChildren(ordermap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    FirebaseDatabase.getInstance().getReference().child("Cartlist").child("User View").child(prevalent.currentonlineuser.getPhone()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                            {
                                                        Toast.makeText(ConfirmFinalOrderActivity.this,"order successfully placed",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(ConfirmFinalOrderActivity.this,HomeActivity.class));
                            }}
                    });
                }
            }
        });

    }
}
