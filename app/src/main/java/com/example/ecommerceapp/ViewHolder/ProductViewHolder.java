package com.example.ecommerceapp.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ecommerceapp.Interface.ItemClickListner;
import com.example.ecommerceapp.R;

public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtproductname,txtproductdescription,txtproductprice;
    public ImageView imageView;
    public ItemClickListner listner;
    public ProductViewHolder(View itemview)
    {
        super(itemview);
        imageView = (ImageView)itemview.findViewById(R.id.user_product_image);
        txtproductname =(TextView)itemview.findViewById(R.id.user_product_name);
        txtproductdescription = (TextView)itemview.findViewById(R.id.user_product_description);
        txtproductprice = (TextView)itemview.findViewById(R.id.user_product_price);
    }

    public void setItemClickListner(ItemClickListner listner)
    {
        this.listner = listner;
    }

    @Override
    public void onClick(View view) {
           listner.onClick(view,getAdapterPosition(),false);
    }
}
